@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-4 justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Edit your Answer!</h2>
                </div>
                <div class="card-body">
                    <form action="{{route('questions.answers.update', [$question->id, $answer->id])}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <input type="hidden" name="body" id="body" value="{{old('body', $answer->body)}}">
                            <trix-editor input="body" class="form-control {{$errors->has('body') ? 'is-invalid' : ''}}"></trix-editor>
                            @error('body')
                                <div class="text-danger">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.0.0/trix.js"></script>
@endsection

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.0.0/trix.css">
@endsection
